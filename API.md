# ShareWon API Document
By Wealthskey

<br>

## Summary
<div style="text-align: justify">
We are now currently using web3.js and lightwallet to assit our service to communicate with the blockchain network.

The API service is to provide a communication channel between the blockchain network and web platform. With the API service, we could be able to create wallet, perform transaction, update ledger and view different transaction.
</div>

## Package
<div style="text-align: justify">
This project is currently relay on different package modules at this stage, in the next stage we will be moving all the function to AWS APIGateway, provide a serverless and package free environment for other parties.

In the future, this service will be powered by AWS APIGateway, all the necessary source will be included on the service side.
</div>

## API Function (With LightWallet)
```function newAddresses(password)```

Create new address for your wallet

```function getBalances()```

Get the balance of current wallet address

```function setSeed()```

Set or update the wallet's seed

```function newWallet()```

Create a wallet

```function showSeed()```

Show the current wallet's seed

```function sendEth()```

Send the token to another wallet, in this example is Ether.

```function functionCall()```

Call the function in specific contract