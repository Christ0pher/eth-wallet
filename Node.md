# Sharewon Blockchain Node Document
By Wealthskey

<br>

# Summary
The Sharewon Blockchain is using Ethereum private chin powered by Amazon Web Services.

# Network Status
User could visit [Network Status Web](http://internal-share-loadb-1mpqko5cpe4lj-504988914.us-east-2.elb.amazonaws.com) to view the network status.

![Network Status](./image/NetStat.jpg)

# Node
In currently we have total of three nodes operating on AWS platform, only the first node will mine(process) the block and the other two node will be syncing the node data in real time.

# Flow
<p style="text-align: center">
  <img src="./image/flow.png" height="50%" width="50%"/>
</p>

# Usage
In current stage the tools are using for testing and development. Network Status, Block Explorer and RPC