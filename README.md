# Sharewon
By Wealthskey

# About Sharewon
<div style="text-align: justify">
Sharewon coin is a ERC20 Token created on Ethereum blockchain network with smart contract. User could buy, sell or transfer Sharewon to their friends, Wealthskey and our partners.

For more about Sharewon, visit [Wealthskey.com](https://www.wealthskey.com/common/specificIssue#collapse1).
</div>

# API
Visit [API Document](./API.md)

# Blockchain Node
Visit [Blockchain Node](./Node.md)