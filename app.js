const express = require('express');
const app = express();
const bodyParser = require('body-parser');

// module.exports = {
//     txutils: require('./lib/txutils.js'),
//     encryption: require('./lib/encryption.js'),
//     signing: require('./lib/signing.js'),
//     keystore: require('./lib/keystore.js'),
//     upgrade: require('./lib/upgrade.js'),
//   };

	app.use(express.static(__dirname));
    app.use(bodyParser.urlencoded({ extended: true }));
    // parse application/json
    // app.use(bodyParser.json());

app.get('/',(req, res) =>
{
	res.sendFile(__dirname + '/index.html');
});

app.listen(3000);
